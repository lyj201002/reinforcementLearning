import copy
import itertools
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from scipy.stats import poisson

lamb_recv1 = 3
lamb_recv2 = 2
lamb_rent1 = 3
lamb_rent2 = 4
max_move = 5
max_cars = 21
gamma = 0.9
theta = 0.01
rent_reward = 10
move_reward = -2
poisson_upper = 11
poisson_cache = dict()
actions = np.arange(-max_move, max_move)


def poisson_probability(lamb, k):
    global poisson_cache
    key = k * 10 + lamb
    if key not in poisson_cache:
        poisson_cache[key] = poisson.pmf(k, lamb)
    return poisson_cache[key]


def init_policy(policy):
    for item in itertools.product(range(max_cars), range(max_cars)):
        i, j = item
        index = max_cars * i + j
        action_upper = min(max_move, i, 20 - j)
        action_lower = min(max_move, j, 20 - i)
        p = action_upper + action_lower + 1
        for k in range(-action_lower, action_upper + 1):
            policy[index][max_move + k] = 1 / p


def evaluation(i, j, state, policy):
    value = state[i][j]
    new_value = 0
    for action in actions:
        tmp1, tmp2 = i - action, j + action
        if policy[max_move + action] == 0:
            continue
        reward = move_reward * abs(action)
        for rent1 in range(poisson_upper):
            rent_possible1 = poisson_probability(lamb_rent1, rent1)
            for rent2 in range(poisson_upper):
                rent_possible2 = poisson_probability(lamb_rent2, rent2)
                rent1 = min(tmp1, rent1)
                rent2 = min(tmp2, rent2)
                new_state = (min(max_cars - 1, tmp1 - rent1 + lamb_recv1),
                             min(max_cars - 1, tmp2 - rent2 + lamb_recv2))
                new_value += (policy[max_move + action] * rent_possible1 * rent_possible2 *
                              (rent_reward * (rent1 + rent2) + reward + gamma * state[new_state[0]][new_state[1]]))

                # new_value += (policy[max_move + action] * rent_possible1 * rent_possible2 * poisson_probability(lamb_recv2, lamb_recv2) *
                #               poisson_probability(lamb_recv1, lamb_recv1) * gamma * state[new_state[0]][new_state[1]])
                # for recv1 in range(max_cars - tmp1):
                #     recv_possible1 = poisson_probability(lamb_recv1, recv1)
                #     for recv2 in range(max_cars - tmp2):
                #         recv_possible2 = poisson_probability(lamb_recv2, recv2)
                #         new_state = tmp1 - rent1 + recv1, tmp2 - rent2 + recv2
                #         new_value += (policy[max_move + action] * recv_possible1 * recv_possible1 *
                #                       gamma * state[new_state[0]][new_state[1]])
    return value, new_value


def policy_iteration():
    iteration = 0
    sum_value = 0
    state = np.zeros((max_cars, max_cars))
    policy = np.zeros((max_cars * max_cars, 2 * max_move + 1))
    init_policy(policy)
    old_action = copy.deepcopy(policy)
    while True:
        stable = True
        # policy evaluation
        while True:
            delta = 0
            tmp_state = copy.deepcopy(state)
            for item in itertools.product(range(max_cars), range(max_cars)):
                i, j = item
                index = i * max_cars + j
                old_state, tmp_state[i][j] = evaluation(i, j, state, policy[index])
                delta = max(abs(state[i][j] - tmp_state[i][j]), delta)
            print(delta)
            state = copy.deepcopy(tmp_state)
            if delta < theta:
                break
        # policy improvement
        print("evaluation over")

        tmp_value = 0
        for item in itertools.product(range(max_cars), range(max_cars)):
            i, j = item
            index = max_cars * i + j
            new_action = np.zeros(2 * max_move + 1)
            value_list = np.zeros(2 * max_move + 1)
            for idx, action in enumerate(old_action[index]):
                if action <= 0:
                    continue
                tmp_policy = np.zeros(2 * max_move + 1)
                tmp_policy[idx] = 1
                _, value_list[idx] = evaluation(i, j, state, tmp_policy)
            tmp_value += np.max(value_list)
            new_action[value_list == value_list.max()] = 1
            new_action /= new_action.sum()
            if (new_action != policy[index]).any():
                stable = False
            policy[index] = new_action
        if not stable and sum_value == tmp_value:
            stable = True
        sum_value = tmp_value
        iteration += 1
        print(iteration)
        policy_img = np.zeros((max_cars, max_cars))
        for item in itertools.product(range(max_cars), range(max_cars)):
            i, j = item
            index = max_cars * i + j
            policy_img[i][j] = policy[index].argmax() + 1
        fig, ax = plt.subplots(figsize=(10, 10))
        sns.heatmap(policy_img, annot=True, square=True, cmap="YlGnBu")
        plt.plot()
        plt.show()
        plt.pause(0)
        if stable:
            return policy, state


if __name__ == "__main__":
    policy, state = policy_iteration()


print("over")
