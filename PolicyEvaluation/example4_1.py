import copy
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


def policy_evaluation():
    p = 0.25
    delta = 1
    theta = 0.01
    gamma = 1
    state = [0.0] * 16
    reward = [-1] * 16
    reward[0] = reward[15] = 0
    iteration = 0
    state_transfer = [
        [0, 0, 1, 2, 4, 4, 5, 6, 8, 8, 9, 10, 12, 12, 13, 15],
        [0, 2, 3, 3, 5, 6, 7, 7, 9, 10, 11, 11, 13, 14, 15, 15],
        [0, 1, 2, 3, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15],
        [0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 12, 13, 14, 15],
    ]
    policy = [[0.25]*4] * 16
    policy[0] = policy[15] = [0] * 4

    fig, ax = plt.subplots(figsize=(10, 10))
    plt.ion()
    while delta > theta:
        iteration += 1
        delta = 0.0
        tmp_state = [0] * 16
        for i in range(16):
            v = state[i]
            for idx, action in enumerate(state_transfer):
                next_state = action[i]
                tmp_state[i] += policy[i][idx] * (reward[i] + gamma * state[next_state])
            delta = max(delta, abs(tmp_state[i] - v))
        state = copy.deepcopy(tmp_state)
        # print(iteration, state)
        plt.title(str(iteration), fontsize=18)
        sns.heatmap(np.reshape(state, (4, 4)).round(1), annot=True, square=True, cmap="YlGnBu")
        plt.plot()
        plt.pause(0.01)
        plt.clf()
    plt.ioff()
    plt.show()
    plt.pause(0)
    plt.close()


if __name__ == "__main__":
    policy_evaluation()
